'use strict'
/**
 * Loads configuration form different data stores before the server starts
 * @module keef/lib/overrides
 * @author Eric Satterwhite
 * @since 0.1.0
 * @requires path
 */

const path = require( 'path' )
const debug = require('debug')('keef:conf:overrides')
const env  = process.env
let pkg = {}

/**
 * @readonly
 * @property {String} PROJECT_ROOT static path to the root of this project
 */
const PROJECT_ROOT = path.normalize(
  env.PROJECT_ROOT
    ? path.resolve(env.PROJECT_ROOT)
    : process.cwd()
)

/**
 * @readonly
 * @property {String} PACKAGE_PATH static path to location on standalone internal packages
 */
const PACKAGE_PATH = env.PACKAGE_PATH
  ? path.resolve(env.PACKAGE_PATH)
  : path.join(path.join(PROJECT_ROOT, 'packages'))

/**
 * @readonly
 * @property {Object} pkg data parsed from the project package.json file
 */
try {
  pkg = require(path.join(PROJECT_ROOT, 'package.json'))
} catch(e) {
  debug('unable to locate package.json in %s', PROJECT_ROOT)
  console.log('no package.json. please run npm init')
  pkg = {}
}

module.exports = {
  PROJECT_ROOT
, PACKAGE_PATH
, pkg
}
