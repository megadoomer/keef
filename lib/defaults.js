'use strict'
/**
 * Predefined project defaults
 * @module keef/lib/defaults
 * @author Eric Satterwhite
 * @since 0.1.0
 */
module.exports = {
  databases: {}
}


